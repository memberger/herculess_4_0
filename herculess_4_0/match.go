package main
import (
	"fmt"
	"log"
)

type Match struct {
	User User `json:"user"`
	Similarity float64 `json:"similarity"`
	Percent float64 `json:"percent"`
	ValidUser bool `json:"validUser"`
}

type Matches []Match

func (m *Match)CalcPercent(min float64, max float64) error {

	var tmin, tmax float64

	if max < min {
		tmax = min
		tmin = max
	}else {
		tmax = max
		tmin = min
	}

	if (m.Similarity < tmin || m.Similarity > tmax) {
		err := fmt.Errorf("similarity not in min max range")
		log.Printf("calcPercent got error: %v", err)
		return err
	}

	m.Percent = (m.Similarity - min) / (max - min) * 100
	return nil
}



type ByDesc []Match

func (m ByDesc) Len() int {
	return  len(m)
}

func (m ByDesc) Swap(i, j int) {
	m[i], m[j] = m[j], m[i]
}

func (m ByDesc) Less(i, j int) bool {
	return m[i].Similarity > m[j].Similarity
}

type ByAsc []Match

func (m ByAsc) Len() int {
	return  len(m)
}

func (m ByAsc) Swap(i, j int) {
	m[i], m[j] = m[j], m[i]
}

func (m ByAsc) Less(i, j int) bool {
	return m[i].Similarity < m[j].Similarity
}