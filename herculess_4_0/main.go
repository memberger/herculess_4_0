package main
import (
	"log"
	"net/http"
	"gopkg.in/mgo.v2/bson"
)

var mongo Mongo
var mqtt Mqtt

func main() {

	//start simulator
	go func() {
		http.Handle("/", http.FileServer(http.Dir("../simulator/dist/")))
		log.Fatal(http.ListenAndServe(":4000", nil))
	}()

	mongoConnectInfo := MongoConnectionInfo{
		Host: "ds035046.mlab.com:35046",
		Database: "da_prototyp",
		Username: "da",
		Password: "da2016!",
	}

	mongo = Mongo{}
	mongo.SetConnectionInfo(mongoConnectInfo)
	mongo.Connect()



	mqttConnectInfo := MqttConnectInfo{
		Host: "tcp://m21.cloudmqtt.com:13574",
		Username: "eikfaugc",
		Password: "9Un1c92qzclq",
	}


	mqtt = Mqtt{}
	mqtt.SetConnectInfo(mqttConnectInfo)
	mqtt.Connect()

	mqttHandlers := MqttHandlers{
		UserReceived: userReceivedHandler,
		UserDelete: userDeleteHandler,
		UserSet: userSetHandler,
		InformationReceived: informationReceivedHandler,
		EvaluationDataReceived: evaluationDataReceivedHandler,
	}

	mqtt.SetHandlers(mqttHandlers)
	log.Fatal(http.ListenAndServe(":13574", nil));
}

func sendAllUsers() error {
	allUsers, err := mongo.AllUsers()
	if err != nil {
		log.Println(err)
		return err
	}

	//send all user
	err = mqtt.SendUsers(allUsers)
	if err != nil {
		log.Println(err)
		return err
	}
	return nil
}



func userReceivedHandler(user User) {

	err := mongo.SaveUser(user)
	if err != nil {
		log.Println(err)
		return
	}
	sendAllUsers()
}



func userDeleteHandler(userId bson.ObjectId) {
	err := mongo.DeleteUser(userId)
	if err != nil {
		log.Println(err)
		return
	}
	sendAllUsers()
}


func userSetHandler(users Users) {
	err := mongo.SetUsers(users)
	if err != nil {
		log.Printf("userSetHandler() got error: %v", err)
	}

	sendAllUsers()
}


func informationReceivedHandler(information Information) {

	users, err := mongo.AllUsers()
	if err != nil {
		log.Println(err)
		return
	}

	result, err := Similarity(information, users)
	if err != nil {
		log.Printf("informationReceivedHandler got error: %v", err)
		return
	}

	err = mqtt.SendResult(result)
	if err != nil {
		log.Println(err)
		return
	}
}

func evaluationDataReceivedHandler(data EvaluationData) {



}

