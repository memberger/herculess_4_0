package main
import (
	"fmt"
	"log"
	"math"
	"sort"
)

const (
	SimMethodNone = 0
	SimMethodEuclidean = 1
	SimMethodScalar = 2
	SimMethodCosine = 3
)

type SimilaritySettings struct {
	Method string `json:"method"`
	Knn int `json:"knn"`
	Threshold float32 `json:"threshold"`
}

type Sim struct {
	Settings SimilaritySettings `json:"settings"`
	Information Information `json:"Information"`
	User User `json:"users"`
	Similarity float32 `json:"similarity"`
}

func Similarity(info Information, users Users) (Result, error) {
	switch info.Method {
		case SimMethodNone:
			err := fmt.Errorf("no similarity method defined")
			log.Printf("Similarity() got error: %v:", err)
			return Result{}, err
		case SimMethodEuclidean:
			return EuclideanResult(info, users)
		case SimMethodScalar:
			return ScalarResult(info, users)
		case SimMethodCosine:
			return CosineResult(info, users)
		default:
			err := fmt.Errorf("invalid similarity methode defined")
			log.Printf("Similarity() got error: %v:", err)
			return Result{}, err
	}
}

func EuclideanResult(info Information, users Users) (Result, error) {
	res := Result{
		Information: info,
		Method: SimMethodEuclidean,
	}

	vecInfo, err := info.HighFeatures()
	if err != nil {
		log.Printf("EuclideanResult() got error: %v", err)
		return res, err
	}

	for _, user := range users {
		match := Match{
			User: user,
		}

		vecUser, err := user.HighFeatures()
		if err != nil {
			log.Printf("EuclideanResult() got error: %v", err)
			return res, err
		}

		sim, err := euclidean(vecInfo, vecUser)
		if err != nil {
			log.Printf("EuclideanResult() got error: %v", err)
			sim = -100
		}

		match.Similarity = sim
		res.Matches = append(res.Matches, match)
	}

	sort.Sort(ByAsc(res.Matches))
	res.CalcMinMax()
	res.CalcMatchesPercent()
	res.FilterMatchesWithKNN()
	res.FilterMatchesWithPercent()
	res.EvaluateUser()
	return res, nil
}

func euclidean(vecA []int8, vecB []int8) (sim float64, err error) {
	sim = 0.0


	if len(vecA) != len(vecB) {
		err = fmt.Errorf("euclidean() failed: different vector lenght")
		return sim, err
	}

	var a, b float64

	count := len(vecA)

	for i := 0; i < count; i++ {

		a = float64(vecA[i])
		b = float64(vecB[i])

		sim += math.Pow(a-b,2)
	}

	sim = math.Sqrt(sim)
	return sim, nil
}

func ScalarResult(info Information, users Users) (Result, error) {
	res := Result{
		Information: info,
		Method: SimMethodScalar,
	}

	vecInfo, err := info.HighFeatures()
	if err != nil {
		log.Printf("ScalarResult() got error: %v", err)
		return res, err
	}

	for _, user := range users {
		match := Match{
			User: user,
		}

		vecUser, err := user.HighFeatures()
		if err != nil {
			log.Printf("ScalarResult() got error: %v", err)
			return res, err
		}

		sim, err := scalar(vecInfo, vecUser)
		if err != nil {
			log.Printf("ScalarResult() got error: %v", err)
			sim = -100
		}

		match.Similarity = sim
		res.Matches = append(res.Matches, match)
	}

	sort.Sort(ByDesc(res.Matches))

	res.CalcMinMax()
	res.CalcMatchesPercent()
	res.FilterMatchesWithKNN()
	res.FilterMatchesWithPercent()
	res.EvaluateUser()

	return res, nil
}

func scalar(vecA []int8, vecB []int8) (sim float64, err error) {
	sim = 0.0

	if len(vecA) != len(vecB) {
		err = fmt.Errorf("euclidean() failed: different vector lenght")
		return sim, err
	}

	var a, b float64

	count := len(vecA)

	for i := 0; i < count; i++ {

		a = float64(vecA[i])
		b = float64(vecB[i])

		sim += a * b
	}

	return sim, nil
}

func CosineResult(info Information, users Users) (Result, error) {
	res := Result{
		Information: info,
		Method: SimMethodCosine,
	}

	vecInfo, err := info.HighFeatures()
	if err != nil {
		log.Printf("CosineResult() got error: %v", err)
		return res, err
	}

	for _, user := range users {
		match := Match{
			User: user,
		}

		vecUser, err := user.HighFeatures()
		if err != nil {
			log.Printf("CosineResult() got error: %v", err)
			return res, err
		}

		sim, err := cosine(vecInfo, vecUser)
		if err != nil {
			log.Printf("CosineResult() got error: %v", err)
			sim = -100
		}

		match.Similarity = sim
		res.Matches = append(res.Matches, match)
	}

	sort.Sort(ByDesc(res.Matches))
	res.CalcMinMax()
	res.CalcMatchesPercent()
	res.FilterMatchesWithKNN()
	res.FilterMatchesWithPercent()
	res.EvaluateUser()
	return res, nil
}

func cosine(vecA []int8, vecB []int8) (sim float64, err error) {
	sim = 0.0

	if len(vecA) != len(vecB) {
		err = fmt.Errorf("cosine similarity failed: different vector lenght")
		return sim, err
	}

	var enumerator, divisor, divisorOne, divisorTwo, a, b float64

	count := len(vecA)

	for i := 0; i < count; i++ {

		a = float64(vecA[i])
		b = float64(vecB[i])

		enumerator += a * b
		divisorOne += math.Pow(a,2)
		divisorTwo += math.Pow(b,2)
	}


	divisor = math.Sqrt(divisorOne) * math.Sqrt(divisorTwo)
	if divisor == 0 {
		fmt.Errorf("cosine similarity failed: divide by 0 error")
		return sim, err
	}

	sim = enumerator / divisor


	return sim, nil
}






