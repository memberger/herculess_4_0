package main
import (

	"fmt"
)

type Information struct {
	Id int `json:"id"`
	Text string `json:"text"`
	FeatureLevel int8 `json:"featureLevel"`
	Features []int8 `json:"features"`
	Method int `json:"method"`
	KNN int `json:"knn"`
	Percent int `json:"percent"`
	EvalUsers []string `json:"evalUsers"`
}

type Informations []Information

func (i Information)HighFeatures() ([]int8, error) {
	if i.FeatureLevel == FeatureLevelHigh {
		return i.Features, nil
	}
	high := make([]int8,3)
	if i.FeatureLevel == FeatureLevelLow {

		vals := []int8{10,30,50,70,90}
		for idx, low := range i.Features {
			high[idx] = vals[low]
		}
		return high, nil
	}

	err := fmt.Errorf("invalid feature or FeatureLevelNone (0)")
	return high, err
}

