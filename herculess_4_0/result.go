package main
import "log"


type Result struct {
	Information Information `json:"information"`
	Method int `json:"method"`
	Matches Matches `json:"matches"`
	MinSim float64 `json:"minSim"`
	MaxSim float64 `json:"maxSim"`
}

func (r *Result)CalcMinMax() {

	var min, max float64

	for idx, m := range r.Matches {
		if idx == 0 {
			min = m.Similarity
			max = m.Similarity
			continue
		}

		if m.Similarity < min {
			min = m.Similarity
		}

		if m.Similarity > max {
			max = m.Similarity
		}
	}

	if r.Method == SimMethodEuclidean {
		r.MaxSim = min
		r.MinSim = max
	}else {
		r.MaxSim = max
		r.MinSim = min
	}
}

func (r *Result)CalcMatchesPercent() error {
	for idx, _ := range r.Matches {
		err := r.Matches[idx].CalcPercent(r.MinSim, r.MaxSim)
		if err != nil {
			log.Printf("CalcMatchesPercent got error: %v", err)
			return err
		}
	}
	return nil
}

func (r *Result)FilterMatchesWithKNN() {

	knn := r.Information.KNN

	if knn <= 0 {
		return
	}

	if knn >= len(r.Matches) {
		return
	}

	r.Matches = r.Matches[:knn]

}

func (r *Result)FilterMatchesWithPercent() {

	percent := float64(r.Information.Percent)
	matches := r.Matches

	for i:=0; i<len(matches); i++ {
		m := r.Matches[i]
		if m.Percent < percent {
			matches = append(matches[:i], matches[i+1:]...)
			i--;
		}
	}

	r.Matches = matches
}

func (r *Result)EvaluateUser() {

	if len(r.Information.EvalUsers) == 0 {
		return
	}



	validUsers := r.Information.EvalUsers
	validUsersMap := map[string]bool{}





	for _, user := range validUsers {
		validUsersMap[user] = true
	}



	for idx, match := range r.Matches {

		if _, ok := validUsersMap[match.User.UserId]; ok {
			r.Matches[idx].ValidUser = true
		}
	}

}

