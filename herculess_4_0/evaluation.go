package main

type EvaluationUser struct {
	User
	Id string `json:"id"`
}
type EvaluationUsers []EvaluationUser

type EvaluationSetting struct {
	Similarity string `json:"similarity"`
	Knn int `json:"knn"`
	Threshold float32 `json:"threshold"`
}

type Evaluation struct {
	InformationId int `json:"informationId"`
	UserIds []string `json:"userIds"`
}
type Evaluations []Evaluation

type EvaluationData struct {
	Settings EvaluationSetting `json:"settings"`
	Evaluations Evaluations `json:"evaluations"`
	Informations Informations `json:"informations"`
	Users EvaluationUsers `json:"users"`
}

func (e *EvaluationData)UserMap() map[string]EvaluationUser {
	userMap := make(map[string]EvaluationUser)
	for _, user := range e.Users {
		userMap[user.Id] = user;
	}
	return userMap
}


