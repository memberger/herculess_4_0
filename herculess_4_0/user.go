package main
import (
	"gopkg.in/mgo.v2/bson"
	"fmt"
)

const(
	FeatureLevelNone = 0
	FeatureLevelLow  = 1
	FeatureLevelHigh = 2
)


type User struct {
	Id bson.ObjectId `json:"id" bson:"_id"`
	UserId string `json:"userId" bson:"user_id"`
	Name string `json:"name" bson:"name"`
	FeatureLevel int8 `json:"featureLevel" bson:"feature_level"`
	Features []int8 `json:"features" bson:"features"`
}

type Users []User

func (u User)HighFeatures() ([]int8, error) {
	if u.FeatureLevel == FeatureLevelHigh {
		return u.Features, nil
	}
	high := make([]int8,3)
	if u.FeatureLevel == FeatureLevelLow {

		vals := []int8{10,30,50,70,90}
		for idx, low := range u.Features {
			high[idx] = vals[low]
		}
		return high, nil
	}

	err := fmt.Errorf("invalid feature or FeatureLevelNone (0)")
	return high, err
}

