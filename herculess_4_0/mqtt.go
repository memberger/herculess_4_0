package main

import (
	MQTT "github.com/eclipse/paho.mqtt.golang"
	"log"
	"encoding/json"
	"gopkg.in/mgo.v2/bson"
)


type MqttConnectInfo struct {
	Host     string
	Username string
	Password string
}

type MqttHandlers struct {
	UserReceived           func(User)
	UserDelete             func(bson.ObjectId)
	UserSet                func(Users)
	InformationReceived    func(Information)
	EvaluationDataReceived func(EvaluationData)
}

type Mqtt struct {
	client      MQTT.Client
	connectInfo MqttConnectInfo
	handlers    MqttHandlers
}

func (mqtt *Mqtt)Connect() error {
	opts := MQTT.NewClientOptions().AddBroker(mqtt.connectInfo.Host)
	opts.SetUsername(mqtt.connectInfo.Username)
	opts.SetPassword(mqtt.connectInfo.Password)
	opts.SetDefaultPublishHandler(mqtt.defaultHandler)
	opts.SetAutoReconnect(true)
	opts.SetConnectionLostHandler(mqtt.connectionLostHandler)
	opts.SetOnConnectHandler(mqtt.connectHandler)

	opts.SetClientID("herculess_dev")

	client := MQTT.NewClient(opts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		return token.Error()
	}

	mqtt.client = client
	mqtt.subscribe()

	return nil
}

func (mqtt *Mqtt)SetConnectInfo(info MqttConnectInfo) {
	mqtt.connectInfo = info
}

func (mqtt *Mqtt)SetHandlers(handlers MqttHandlers) {
	mqtt.handlers = handlers
}

func (mqtt *Mqtt)subscribe() {
	handlers := []handler{
		newHandler("/user/add", 2, mqtt.createUserHandler),
		newHandler("/user/delete", 2, mqtt.deleteUserHandler),
		newHandler("/user/set", 2, mqtt.setUserHandler),
		newHandler("/information", 2, mqtt.informationReceivedHandler),
		newHandler("/evaluation/data", 2, mqtt.evaluationDataReceivedHandler),
	}

	for _, handler := range handlers {
		if token := mqtt.client.Subscribe(handler.topic, handler.qos, handler.callback); token.Wait() && token.Error() != nil {
			log.Println(token.Error())
		}
	}

}

func (mqtt *Mqtt)createUserHandler(client MQTT.Client, msg MQTT.Message) {

	user := User{}

	err := json.Unmarshal(msg.Payload(), &user)
	if err != nil {
		log.Println("parse user payload failed")
		return
	}
	mqtt.handlers.UserReceived(user)
}

func (mqtt *Mqtt)deleteUserHandler(client MQTT.Client, msg MQTT.Message) {
	req := struct {
		Id bson.ObjectId `json:"id"`
	}{}
	err := json.Unmarshal(msg.Payload(), &req)
	if err != nil {
		log.Println(err)
		return
	}

	if req.Id == "" {
		log.Println("user delete failed: no user id")
	}

	mqtt.handlers.UserDelete(req.Id)
}

func (mqtt *Mqtt)setUserHandler(client MQTT.Client, msg MQTT.Message) {
	var users Users

	err := json.Unmarshal(msg.Payload(), &users)
	if err != nil {
		log.Printf("parse user payload got error: %v", err)
		return
	}
	mqtt.handlers.UserSet(users)
}

func (mqtt *Mqtt)informationReceivedHandler(client MQTT.Client, msg MQTT.Message) {
	information := Information{}
	err := json.Unmarshal(msg.Payload(), &information)
	if err != nil {
		log.Println("parse user payload failed")
		return
	}
	log.Printf("information received: %v", information)
	mqtt.handlers.InformationReceived(information)
}

func (mqtt *Mqtt)evaluationDataReceivedHandler(client MQTT.Client, msg MQTT.Message) {
	evaluationData := EvaluationData{}
	err := json.Unmarshal(msg.Payload(), &evaluationData)
	if err != nil {
		log.Println(err)
		log.Println("parse evaluation data payload failed")
		return
	}
	mqtt.handlers.EvaluationDataReceived(evaluationData)
}

func (mqtt *Mqtt)defaultHandler(client MQTT.Client, msg MQTT.Message) {

}

func (mqtt *Mqtt)connectionLostHandler(client MQTT.Client, err error) {
	log.Println(client)
	log.Println(err)
}

func (mqtt *Mqtt)connectHandler(client MQTT.Client) {
	log.Println(client)
	log.Println("connect handler")
}

func (mqtt *Mqtt)SendUsers(users Users) error {
	return mqtt.send("/user/all", users, true)
}

func (mqtt *Mqtt)SendResult(result Result) error {
	return mqtt.send("/result", result, false)
}

func (mqtt *Mqtt)send(topic string, data interface{}, keep bool) error {
	payload, err := json.Marshal(data)
	if err != nil {
		log.Println(err)
		return err
	}

	mqtt.client.Publish(topic, 2, true, payload)
	return nil
}


// Handler

type handler struct {
	topic    string
	qos      byte
	callback func(MQTT.Client, MQTT.Message)
}

func newHandler(topic string, qos byte, callback func(MQTT.Client, MQTT.Message)) handler {
	return handler{
		topic: topic,
		qos: qos,
		callback: callback,
	}
}