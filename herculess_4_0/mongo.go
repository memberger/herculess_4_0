package main
import (
	"gopkg.in/mgo.v2"
	"time"
	"log"
	"gopkg.in/mgo.v2/bson"
)

const (
	UserColName = "user"
)

type MongoConnectionInfo struct {
	Host string
	Database string
	Username string
	Password string
}

type connection struct {
	Session    *mgo.Session
	Collection *mgo.Collection
}

func (c *connection)Close() {
	c.Session.Close()
}

type Mongo struct {
	connectionInfo MongoConnectionInfo
	session *mgo.Session
}

func (m *Mongo)Connect() error {
	dialInfo := mgo.DialInfo{
		Addrs: []string{m.connectionInfo.Host},
		Timeout: 1 * time.Second,
		Database: m.connectionInfo.Database,
		Username: m.connectionInfo.Username,
		Password: m.connectionInfo.Password,
	}

	s, err := mgo.DialWithInfo(&dialInfo)
	if err != nil {
		log.Println("create mongo session failed")
		return err
	}

	s.SetMode(mgo.Monotonic, true)
	m.session = s

	return nil
}

func (m *Mongo)CloseConnection() {
	m.session.Close()
}

func (m *Mongo)SetConnectionInfo(info MongoConnectionInfo) {
	m.connectionInfo = info
}

func (m *Mongo)setupCollections() {
	m.setupUserCollection()
}

func (m *Mongo)sessionCopy() *mgo.Session {
	return m.session.Copy()
}

func (m *Mongo)newConnection(colName string) *connection  {
	conn := &connection{}
	conn.Session = m.sessionCopy()
	conn.Collection = conn.Session.DB(m.connectionInfo.Database).C(colName)
	return conn
}


// User

// Setup User Collection
func (m *Mongo)setupUserCollection() {
	conn := m.newConnection(UserColName)
	defer conn.Close()

	indexes := []mgo.Index{
		{
			Key:        []string{"_id"},
			Unique:     true,
			Background: true,
		},
	}

	for _, index := range indexes {
		err := conn.Collection.EnsureIndex(index)
		if err != nil {
			log.Panicf("setup user collenction indexes failed: %s", err)
		}
	}
}


func (m *Mongo)AllUsers() (users Users, err error) {
	conn := m.newConnection(UserColName)
	defer conn.Close()
	err = conn.Collection.Find(nil).All(&users)
	return users, err
}

func (m *Mongo)SaveUser(u User) error {
	conn := m.newConnection(UserColName)
	defer conn.Close()

	if u.Id.Hex() == "" {
		u.Id = bson.NewObjectId()
	}

	// insert
	info, err := conn.Collection.UpsertId(u.Id, u)
	if err != nil {
	log.Println(err)
	}
	log.Println(info)

	return nil
}

func (m *Mongo)SetUsers(users Users) error {
	conn := m.newConnection(UserColName)
	defer conn.Close()

	_,err := conn.Collection.RemoveAll(nil)
	if err != nil {
		log.Printf("Mongo.SetUsers() got error: %v", err)
		return err
	}





	for _, user := range users {
		user.Id = bson.NewObjectId()
		err = conn.Collection.Insert(&user)
		if err != nil {
			log.Printf("Mongo.SetUsers() got error: %v", err)
			return err
		}
	}




	return nil
}

func (m *Mongo)DeleteUser(id bson.ObjectId) error {
	conn := m.newConnection(UserColName)
	defer conn.Close()
	return conn.Collection.RemoveId(id)
}

