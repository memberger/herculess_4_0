#!/usr/bin/env bash

cd ../herculess_4_0
go build -o ../builds/herculess_4_0_darwin
GOARCH=arm GOOS=linux go build -o ../builds/herculess_4_0_linux_arm
GOARCH=amd64 GOOS=linux go build -o ../builds/herculess_4_0_linux_amd64