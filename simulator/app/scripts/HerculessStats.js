function HerculessStats(data) {
  data = data || {};

  console.log(data);

  this.informations = data.informations;
  this.users = data.users;
  this.informationCount = 0;
  this.informationUsersCount = 0;
  this.allUsers = 0;
  this.allInformations = 0;
  this.sentUsers = 0;
  this.unsentUsers = 0;
  this.validUsers = 0;
  this.invalidUsers = 0;
  this.validResults = 0;
  this.invalidResults = 0;


  if (this.informations && this.users) {
    //calculate max values
    this.zMax = this.informations.length * this.users.length;
    var fMax = 0;
    var evalUsersCount = 0;
    var totalUsersCount = this.informations.length;
    var info;
    for (var i = 0, len = this.informations.length; i < len; i++) {
      info = this.informations[i];
      if (!Array.isArray(info.evalUsers)) {
        alert("Datensatz ist nicht für die Evaluierung geeignet, da nicht für jeder Information, Personen für die Evaluierung definiert wurden!");
        return;
      }
      evalUsersCount += info.evalUsers.length;
    }
    var failUserCount = this.zMax - evalUsersCount;

    this.fMax = failUserCount;
  }


}

HerculessStats.prototype = {
  setInformationCount: function (count) {
    this.informationCount = count;
  },
  setInformationUsersCount: function (count) {
    this.informationUsersCount = count;
  },
  addResult: function (result) {
    this.informationCount--;
    this.allInformations++;
    this.allUsers += this.informationUsersCount;

    var matches = result.matches;
    var match;
    var hasValid = false;
    for (var i = 0, len = matches.length; i < len; i++) {
      match = matches[i];
      this.sentUsers++;
      if (match.validUser) {
        hasValid = true;
        this.validUsers++;
      } else {
        this.invalidUsers++;
      }
    }

    if (hasValid) {
      this.validResults++;
    } else {
      this.invalidResults++;
    }

    this.unsentUsers = this.allUsers - this.sentUsers;


  },
  reset: function () {
    this.informationCount = 0;
    this.informationUsersCount = 0;
    this.allUsers = 0;
    this.allInformations = 0;
    this.sentUsers = 0;
    this.unsentUsers = 0;
    this.validUsers = 0;
    this.invalidUsers = 0;
    this.validResults = 0;
    this.invalidResults = 0;
  },
  getStats: function () {

    //calculate percentage
    var validResultsPercent = (this.validResults - 0) / (this.allInformations - 0) * 100;
    var invalidResultsPercent = (this.invalidResults - 0) / (this.allInformations - 0) * 100;

    var sentUsersPercent = (this.sentUsers - 0) / (this.allUsers - 0) * 100;
    var unsentUsersPercent = (this.unsentUsers - 0) / (this.allUsers - 0) * 100;

    var validUsersPercent = (this.validUsers - 0) / (this.sentUsers - 0) * 100;
    var invalidUsersPercent = (this.invalidUsers - 0) / (this.sentUsers - 0) * 100;

    var invalidUsersTotalPercent = (this.invalidUsers - 0) / (this.allUsers - 0) * 100;

    var fRed = 100 - (this.invalidUsers * 100 / this.fMax);
    var zRed = 100 - (this.sentUsers * 100 / this.zMax);

    return {
      allUsers: this.allUsers,
      allUsersPercent: 100,
      allInformations: this.allInformations,
      allInformationsPercent: 100,
      sentUsers: this.sentUsers,
      sentUsersPercent: sentUsersPercent,
      unsentUsers: this.unsentUsers,
      unsentUsersPercent: unsentUsersPercent,
      validUsers: this.validUsers,
      validUsersPercent: validUsersPercent,
      invalidUsers: this.invalidUsers,
      invalidUsersPercent: invalidUsersPercent,
      invalidUsersTotalPercent: invalidUsersTotalPercent,
      validResults: this.validResults,
      validResultsPercent: validResultsPercent,
      invalidResults: this.invalidResults,
      invalidResultsPercent: invalidResultsPercent,
      fMax: this.fMax,
      fRed: fRed,
      zMax: this.zMax,
      zGes: this.sentUsers,
      zRed: zRed
    }
  }


}

