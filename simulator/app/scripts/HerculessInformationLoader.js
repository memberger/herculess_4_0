function HerculessJsonLoader(data) {
    data = data || {};

    this.defaultInformationJsonPath = data.defaultInformationJsonPath || 'information.default.json';
    this.defaultUserJsonPath = data.defaultUserJsonPath || 'user.default.json';
    this.defaultEvaluationJsonPath = data.defaultEvaluationJsonPath || 'evaluation.default.json';
}

HerculessJsonLoader.prototype = {

    load: function (path, success, error) {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (this.readyState === 4 && this.status === 200) {
                var res = JSON.parse(this.responseText);
                if (!res) {
                    var err = new HerculessError({desc: 'parse information json failed'});
                    error(err);
                }
                success(res);

            } else if (this.readyState === 4 && this.status !== 200) {

                error('xhttp request failed');
            }
        }

        xhttp.open('GET', path, true);
        xhttp.send();
    },
    loadDefaultInformation: function (success, error) {
        var that = this;
        this.load(this.defaultInformationJsonPath, function(res) {
            var informations = HerculessInformation.informationsFromData(res,  true);
            success(informations);
        }, function(err) {
            var herculessErr = new HerculessError({
                desc: 'load default information failed. no file found for defaultJSONPath: ' + that.defaultInformationJsonPath,
                err: err
            });
            error(herculessErr);
        });
    },
    loadDefaultUser: function (success, error) {
        var that = this;
        this.load(this.defaultUserJsonPath, function(res) {
            var users = HerculessUser.usersFromData(res, true);
            success(users);
        }, function(err) {
            var herculessErr = new HerculessError({
                desc: 'load default users failed. no file found for defaultJSONPath: ' + that.defaultUserJsonPath,
                err: err
            });
            error(herculessErr);
        });
    },
    loadDefaultEvaluation: function (success, error) {
        var that = this;
        this.load(this.defaultEvaluationJsonPath, function(res) {
           var evaluations = HerculessEvaluation.evaluationsFromData(res);
            success(evaluations);
        }, function(err) {
            var herculessErr = new HerculessError({
                desc: 'load default evaluations failed. no file found for defaultEvaluationJsonPath: ' + that.defaultEvaluationPath,
                err: err
            })
            error(herculessErr);
        });
    },
    readFromFile: function (file, success, error) {
        var reader = new FileReader();
        reader.onload = function () {
            var data;
            try {
                data = JSON.parse(reader.result)
                if (!data) {
                    var err = new HerculessError({desc: 'parse information json failed'});
                    error(err);
                }
            } catch (e) {
                var err = new HerculessError({desc: 'read informations json file failes', err: e});
                error(err);
            }


            success(data);
        };
        reader.readAsText(file);
    }
}
