'use strict';

/**
 * @ngdoc overview
 * @name simulatorApp
 * @description
 * # simulatorApp
 *
 * Main module of the application.
 */
angular
  .module('simulatorApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/evaluation', {
        templateUrl: 'views/evaluation.html',
        controller: 'EvaluationCtrl',
        controllerAs: 'evaluation'
      })
      .when('/users', {
        templateUrl: 'views/users.html',
        controller: 'UsersCtrl',
        controllerAs: 'users'
      })
      .when('/informations', {
        templateUrl: 'views/informations.html',
        controller: 'InformationsCtrl',
        controllerAs: 'informations'
      })
      .when('/simulator', {
        templateUrl: 'views/simulator.html',
        controller: 'SimulatorCtrl',
        controllerAs: 'simulator'
      })
      .when('/monitor', {
        templateUrl: 'views/monitor.html',
        controller: 'MonitorCtrl',
        controllerAs: 'monitor'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
