
// Herculess Model

function HerculessModel() {}

HerculessModel.objectsFromData = function(data, Model, convertFeatures) {
    if (!Array.isArray(data) || !Model)
        return;

    var objs = [];
    for (var i = 0, len = data.length; i < len; i++) {
        var obj = new Model(data[i]);
        /*if (convertFeatures) {
            this.convertObjectFeatures(obj);
        }*/
        objs.push(obj);
    }

    return objs;
};

HerculessModel.convertObjectFeatures = function(obj) {
    if (obj.features !== undefined) {
        var val;
        for (var j = 0, jlen = obj.features.length; j < jlen; j++) {
            val = obj.features[j];
            switch (val) {
                case 0:
                    obj.features[j] = 10;
                    break;
                case 1:
                    obj.features[j] = 30;
                    break;
                case 2:
                    obj.features[j] = 50;
                    break;
                case 3:
                    obj.features[j] = 70;
                    break;
                case 4:
                    obj.features[j] = 90;
                    break;
                default :
                    break;
            }
        }
    }
}


// Herculess User

function HerculessUser(data) {
    data = data || {};
    this.id = data.id;
    this.userId = data.userId;
    this.name = data.name;
    this.features = data.features;
    this.featureLevel = data.featureLevel;
}

HerculessUser.prototype = {
    payload: function() {
        var obj = {
            id: this.id,
            userId: this.userId,
            name: this.name,
            features: this.features,
            featureLevel: this.featureLevel
        };

        return JSON.stringify(obj);
    },
}

HerculessUser.arrayPayload = function(data) {
  return JSON.stringify(data);
}

HerculessUser.usersFromData = function(data, convertFeatures) {
    return HerculessModel.objectsFromData(data, HerculessUser, convertFeatures);
}



// Herculess Information

function HerculessInformation(data) {
    data = data || {};
    this.id = data.id;
    this.text = data.text;
    this.features = data.features;
    this.featureLevel = data.featureLevel;
    this.method = data.method;
    this.knn = data.knn;
    this.percent = data.percent;
    this.evalUsers = data.evalUsers;
}

HerculessInformation.informationsFromData = function(data, convertFeatures) {
    var informations = HerculessModel.objectsFromData(data, HerculessInformation, convertFeatures);
    for (var i = 0, len = informations.length; i < len; i++) {
        informations[i].id = i+1;
    }
    return informations;
}

HerculessInformation.prototype = {
    payload: function() {
        var obj = {
            id: this.id,
            text: this.text,
            features: this.features,
            method: this.method,
            featureLevel: this.featureLevel,
            knn: this.knn,
            percent: this.percent,
            evalUsers: this.evalUsers
        };

        return JSON.stringify(obj);
    },
    setMethod: function(m) {
      this.method = parseInt(m)
    },
    setKNN: function(knn) {
      this.knn = parseInt(knn)
    },
    setPercent: function(percent) {
      this.percent = parseInt(percent)
    }
};


// Herculess Match

function HerculessMatch(data) {
    data = data || {};
    this.user = new HerculessUser(data.user);
    this.similarity = data.similarity;
    this.percent = data.percent;
    this.validUser = data.validUser;
}

HerculessMatch.matchesFromData = function(data) {
    return HerculessModel.objectsFromData(data, HerculessMatch);
};



// Herucless Result

function HerculessResult(data) {
    data = data || {};
    this.information = new HerculessInformation(data.information);
    this.matches = HerculessMatch.matchesFromData(data.matches);
    this.minSim = data.minSim;
    this.maxSim = data.maxSim;
}

HerculessResult.resultsFromData = function(data) {
    return HerculessModel.objectsFromData(data, HerculessResult);
};


// Herculess Evaluation

function HerculessEvaluation(data) {
    data = data || {};
    this.informationId = data.informationId;
    this.userIds = data.userIds;
}

HerculessEvaluation.evaluationsFromData = function(data) {
    return HerculessModel.objectsFromData(data, HerculessEvaluation);
}

function HerculessEvalauationSetting(data) {
    data = data || {};
    this.similarity = data.similarity;
    this.knn = data.knn;
    this.threshold = data.threshold;
}

function HerculessEvaluationData(data) {
    data = data || {};
    this.settings = data.settings;
    this.evaluations = data.evaluations;
    this.informations = data.informations;
    this.users = data.users;
}

HerculessEvaluationData.prototype = {
    payload: function() {
        return JSON.stringify(this);
    }
}

