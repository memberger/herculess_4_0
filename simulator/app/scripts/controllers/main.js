'use strict';

/**
 * @ngdoc function
 * @name simulatorApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the simulatorApp
 */
angular.module('simulatorApp')
  .controller('MainCtrl', function ($scope, $rootScope) {

      $scope.users = [];
      $scope.results = [];
      $scope.delayTime = '0';

      // setup mqtt connection over HerculessMqtt


      var info = new HerculessMqttConnectInfo({
        host: 'm21.cloudmqtt.com',
        port: 33574,
        username: "eikfaugc",
        password: "9Un1c92qzclq"
      });

      var herculessMqtt = new HerculessMqtt(info);
      $rootScope.herculessMqtt = herculessMqtt;

      herculessMqtt.connect(function(success){
        console.log('HerculessMqttClient connected');
      }, function(err) {
        console.log(err);
      });


      //define HerculessMqtt Callback functions



      herculessMqtt.onResultUsersReceived(function(users) {
        console.log(users);
        $scope.users = users;
        $scope.$apply();
      });




















      $scope.sendInfos = function() {
        $scope.results = [];

        //set methode on informations
        for (var i= 0, len=$scope.informations.length; i<len; i++) {
          $scope.informations[i].setMethod($scope.method);
          $scope.informations[i].setKNN($scope.knn);
          $scope.informations[i].setPercent($scope.percent);
        }

        var delayMillis = parseInt($scope.delayTime) * 1000;
        herculessMqtt.sendInformationsWithDelay($scope.informations, delayMillis);
      };

  });



