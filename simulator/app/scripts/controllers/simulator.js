'use strict';

/**
 * @ngdoc function
 * @name simulatorApp.controller:SimulatorCtrl
 * @description
 * # SimulatorCtrl
 * Controller of the simulatorApp
 */
angular.module('simulatorApp')
  .controller('SimulatorCtrl', function ($scope, $rootScope) {
    $scope.delayTime = '0';
    $scope.results = [];

    var stats;

    var info = new HerculessMqttConnectInfo({
      host: 'm21.cloudmqtt.com',
      port: 33574,
      username: "eikfaugc",
      password: "9Un1c92qzclq"
    });

    var herculessMqtt = new HerculessMqtt(info);

    herculessMqtt.connect(function(success){
      console.log('HerculessMqttClient connected');
    }, function(err) {
      console.log(err);
    });


    herculessMqtt.onResultUsersReceived(function(users) {
      $rootScope.users = users;
      stats = new HerculessStats({
        informations: $rootScope.informations,
        users: $rootScope.users
      });
      $scope.$apply();
    });

    herculessMqtt.onResultReceived(function(result) {
      $scope.results.push(result);
      stats.addResult(result);
      $scope.stats = stats.getStats();
      $scope.$apply();
    });



    var infoLoader = new HerculessJsonLoader({
      defaultJSONPath: 'information.default.json'
    });

    if (!$rootScope.informations) {
      infoLoader.loadDefaultInformation(function(informations) {
        $rootScope.informations = informations;
        stats = new HerculessStats({
          informations: $rootScope.informations,
          users: $rootScope.users
        });
      }, function(err) {
        console.log(err);
      });
    }






    $scope.sendInfos = function() {
      $scope.results = [];

      //set methode on informations
      for (var i= 0, len=$scope.informations.length; i<len; i++) {
        $rootScope.informations[i].setMethod($scope.method);
        $rootScope.informations[i].setKNN($scope.knn);
        $rootScope.informations[i].setPercent($scope.percent);
      }

      stats.reset();
      stats.setInformationCount($rootScope.informations.length);
      stats.setInformationUsersCount($rootScope.users.length);

      var delayMillis = parseInt($scope.delayTime) * 1000;
      herculessMqtt.sendInformationsWithDelay($rootScope.informations, delayMillis);
    };


  });

