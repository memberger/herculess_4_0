'use strict';

/**
 * @ngdoc function
 * @name simulatorApp.controller:InformationsCtrl
 * @description
 * # InformationsCtrl
 * Controller of the simulatorApp
 */
angular.module('simulatorApp')
  .controller('InformationsCtrl', function ($scope, $rootScope) {

    $scope.informations = [];
    $rootScope.informations = [];

    if ($rootScope.users) {
      $scope.userMap = userMapFromUsers($rootScope.users);
    }

    var infoLoader = new HerculessJsonLoader({
      defaultJSONPath: 'information.default.json'
    });

    console.log(infoLoader)

    infoLoader.loadDefaultInformation(function(informations) {
      setInformations(informations)

    }, function(err) {
      console.log(err);
    });


    $scope.infoFileInputChanged = function(evt) {
      var file = evt.files[0];
      if (!file)
        return;

      infoLoader.readFromFile(file, function (data) {
        var informations = HerculessInformation.informationsFromData(data)
        setInformations(informations);
        $scope.$apply();
      }, function (err) {
        console.log(err);
      });
    };


    function setInformations(infos) {
      $scope.informations = infos;
      $rootScope.informations = infos;
    }



    var info = new HerculessMqttConnectInfo({
      host: 'm21.cloudmqtt.com',
      port: 33574,
      username: "eikfaugc",
      password: "9Un1c92qzclq"
    });

    var herculessMqtt = new HerculessMqtt(info);

    herculessMqtt.connect(function(success){
      console.log('HerculessMqttClient connected');
    }, function(err) {
      console.log(err);
    });


    herculessMqtt.onResultUsersReceived(function(users) {
      $rootScope.users = users;
      $scope.userMap = userMapFromUsers(users);
      $scope.$apply();
    });


    function userMapFromUsers(users) {
      var userMap = {};
      var user;
      for (var i = 0, len = users.length; i < len; i++) {
        user = users[i];
        userMap[user.userId] = user;
      }
      return userMap;
    }



  });
