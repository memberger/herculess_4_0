'use strict';

/**
 * @ngdoc function
 * @name simulatorApp.controller:EvaluationCtrl
 * @description
 * # EvaluationCtrl
 * Controller of the simulatorApp
 */
angular.module('simulatorApp')
  .controller('EvaluationCtrl', function ($scope) {

    var info = new HerculessMqttConnectInfo({
      host: 'm21.cloudmqtt.com',
      port: 33574,
      username: "eikfaugc",
      password: "9Un1c92qzclq"
    });

    var herculessMqtt = new HerculessMqtt(info);

    herculessMqtt.connect(function(success){
      console.log('HerculessMqttClient connected');
    }, function(err) {
      console.log(err);
    });


    $scope.sendEvaluationData = function() {
      var settings = new HerculessEvalauationSetting({
        similarity: 'cosine',
        knn: 3,
        threshold: 0
      })

      $scope.evaluationData.settings = settings;
      console.log($scope.evaluationData);
      herculessMqtt.sendEvaluationData($scope.evaluationData);

    }



    var loader = new HerculessJsonLoader({});

    $scope.evaluationData = new HerculessEvaluationData();

    loader.loadDefaultInformation(function(informations) {
      $scope.informations = informations;
      $scope.evaluationData.informations = informations;
      console.log($scope.informations);

    }, function(err) {
      console.log(err);
    });

    loader.loadDefaultUser(function(users) {
      $scope.users = users;
      $scope.evaluationData.users = users;
      console.log($scope.users);
    }, function(err) {
      console.log(err);
    });

    loader.loadDefaultEvaluation(function(evaluations) {
      $scope.evaluations = evaluations;
      $scope.evaluationData.evaluations = evaluations;
      console.log($scope.evaluations);
    }, function(err) {
      console.log(err);
    });











  });
