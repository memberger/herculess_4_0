'use strict';

/**
 * @ngdoc function
 * @name simulatorApp.controller:UsersCtrl
 * @description
 * # UsersCtrl
 * Controller of the simulatorApp
 */
angular.module('simulatorApp')
  .controller('UsersCtrl', function ($scope, $rootScope) {

    if ($rootScope.users) {
      $scope.users = $rootScope.users;
    }else {
      $scope.users = [];
      $rootScope.users = [];
    }



    var info = new HerculessMqttConnectInfo({
      host: 'm21.cloudmqtt.com',
      port: 33574,
      username: "eikfaugc",
      password: "9Un1c92qzclq"
    });

    var herculessMqtt = new HerculessMqtt(info);


    herculessMqtt.connect(function(success){
      console.log('HerculessMqttClient connected');
    }, function(err) {
      console.log(err);
    });


    herculessMqtt.onResultUsersReceived(function(users) {
      $scope.users = users;
      $rootScope.users = users;
      $scope.$apply();
    });

    $scope.addUserClicked = function() {
      var user = new HerculessUser({
        userId: $scope.newUser.id,
        name: $scope.newUser.name
      });

      var featureLevel = parseInt($scope.newUser.featureLevel);
      var production = parseInt($scope.newUser.production);
      var maintenance = parseInt($scope.newUser.maintenance);
      var management = parseInt($scope.newUser.management);

      if (featureLevel === 1) {
        if (production < 0 || production > 4 || !production) {
          alert("Leider ist ein Fehler aufgetreten. Ungültiges Merkmal Produktion")
          return;
        }

        if (maintenance < 0 || maintenance > 4 || !production) {
          alert("Leider ist ein Fehler aufgetreten. Ungültiges Merkmal Wartung")
          return;
        }

        if (management < 0 || management > 4 || !production) {
          alert("Leider ist ein Fehler aufgetreten. Ungültiges Merkmal Management")
          return;
        }
      }else if(featureLevel === 2) {
        if (production < 0 || production > 99 || !production) {
          alert("Leider ist ein Fehler aufgetreten. Ungültiges Merkmal Produktion")
          return;
        }

        if (maintenance < 0 || maintenance > 99 || !production) {
          alert("Leider ist ein Fehler aufgetreten. Ungültiges Merkmal Wartung")
          return;
        }

        if (management < 0 || management > 99 || !production) {
          alert("Leider ist ein Fehler aufgetreten. Ungültiges Merkmal Management")
          return;
        }
      }else {
        alert("Leider ist ein Fehler aufgetreten. Ungültiger Merkmalsbereich");
        return;
      }


      var features = [production,maintenance,management]
      user.featureLevel = featureLevel;
      user.features = features;

      herculessMqtt.sendUserAdd(user);
      $scope.newUser = {};
    };

    $scope.deleteUserClicked = function(user) {
      herculessMqtt.sendUserDelete(user.id);
    };


    var userLoader = new HerculessJsonLoader({
      defaultJSONPath: 'user.default.json'
    })

    $scope.userFileInputChanged = function(evt) {
      console.log('file input changed');
      var file = evt.files[0];
      if (!file)
        return;

      userLoader.readFromFile(file, function (data) {
        var users = HerculessUser.usersFromData(data);
        console.log(users);

        herculessMqtt.sendUserSet(users)
        angular.element("input[type='file']").val(null);

      }, function(err) {
        console.log(err)
      });
    };

  });
