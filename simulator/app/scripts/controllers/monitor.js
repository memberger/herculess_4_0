'use strict';

/**
 * @ngdoc function
 * @name simulatorApp.controller:MonitorCtrl
 * @description
 * # MonitorCtrl
 * Controller of the simulatorApp
 */
angular.module('simulatorApp')
  .controller('MonitorCtrl', function () {
    this.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
