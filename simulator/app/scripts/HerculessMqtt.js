function HerculessMqtt(connectInfo) {

    this.callback = {};
    this.connectInfo = connectInfo;
    this.client = new Paho.MQTT.Client(this.connectInfo.host, 33574, '/', this.connectInfo.clientId);

    var that = this;



    var onMessageArrived = function(message) {
        var topic = message.destinationName;
        var payloadObj = JSON.parse(message.payloadString);

        switch (topic) {
            case '/user/all':
                var users = HerculessUser.usersFromData(payloadObj);
                if (that.callback.onUsersReceived) {
                    that.callback.onUsersReceived(users);
                }
                break;
            case '/result':
                var result = new HerculessResult(payloadObj);
                if (that.callback.onResultReceived) {
                    that.callback.onResultReceived(result);
                }

                break;
            default:
                return;
        };

    };

    var onConnectionLost = function(msg) {
      console.log('connection lost');
      console.log(msg);
    };




    this.onResultReceived = function(c) {
        this.callback.onResultReceived = c;
    };

    this.onResultUsersReceived = function(c) {
        this.callback.onUsersReceived = c;
    };

    this.sendInformation = function(information) {
        var msg = newPayloadMessage(information, '/information');
        if (msg) {
            this.client.send(msg);
        }
    };

    this.sendInformationsWithDelay = function(informations, delay) {
        var i = 0, len=informations.length;
        this.sendInformation(informations[i]);
        i++;

        var that = this;
        setInterval(function(){

            if (i < len) {
                console.log("send information");
                that.sendInformation(informations[i])
                i++;
            }else {
                clearInterval(this);
            }
        }, delay);
    };

    this.sendUserAdd = function(user) {
        var msg = newPayloadMessage(user, '/user/add');
        if (msg) {
            this.client.send(msg);
        }
    };

    this.sendUserDelete = function(userId) {
        var msg = newMessage({ id: userId }, '/user/delete');
        if (msg) {
            this.client.send(msg);
        }
    };

    this.sendUserSet = function(users) {
      var msg = newPayloadMessage(users, '/user/set');
      if (msg) {
        this.client.send(msg)
      }
    }

    this.sendEvaluationData = function(evaluationData) {
      var msg = newPayloadMessage(evaluationData, '/evaluation/data');
        if (msg) {
            this.client.send(msg);
        }
    };


    this.connect = function(success, error) {
        // connect the client
        var that = this;
        this.client.connect({
            useSSL: connectInfo.useSSL,
            userName: connectInfo.username,
            password: connectInfo.password,
            onSuccess: function (info) {
                that.client.subscribe("/result");
                that.client.subscribe("/user/all");
                that.client.onMessageArrived = onMessageArrived;
                that.client.onConnectionLost = onConnectionLost;
                success(info);

            },
            onFailure: function (e) {
                var err = new HerculessError('connect to broker failed', e);
                error(err);
            }
        });
    };



    function newPayloadMessage(data, destinationName) {

        var payload;

        if (Array.isArray(data)) {
          payload = HerculessUser.arrayPayload(data)
        }else {
          if (typeof data.payload !== 'function') {
            console.log('unable to send message. data does not implement payload methode');
            return;
          }
          payload = data.payload();
        }

        var msg = new Paho.MQTT.Message(payload);
        msg.destinationName = destinationName;

        return msg;
    }


    function newMessage(data, destinationName) {
        var msg = new Paho.MQTT.Message(JSON.stringify(data));
        msg.destinationName = destinationName;
        return msg;
    }

}



function HerculessMqttConnectInfo(obj) {
    this.host = obj.host;
    this.port = obj.port;
    this.username = obj.username;
    this.password = obj.password;
    this.clientId = obj.clientId || uuid();
    this.useSSL = obj.useSSL || true;


    function uuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
            return v.toString(16);
        });
    }



    this.valid = function() {
        var err;
        if (!this.host) {
            err = 'no host in ConnectionInfo';
            herculessMqttLogger.error(err);
            return false;
        }

        if (!this.port) {
            err = 'no port in ConnectionInfo';
            herculessMqttLogger.error(err);
            return false;
        }

        if (!this.username) {
            err = 'no username in ConnectionInfo';
            herculessMqttLogger.error(err);
            return false;
        }

        if (!this.password) {
            err = 'no password in ConnectionInfo';
            herculessMqttLogger.error(err);
            return false;
        }

        return true;
    };
}



function HerculessMqttLogger() {}

HerculessMqttLogger.prototype = {
    error: function(msg) {
        console.log('HerculessError: ' + msg);
    }
}

var herculessMqttLogger = new HerculessMqttLogger();
